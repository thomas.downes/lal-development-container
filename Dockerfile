FROM debian:stretch

ARG INSTALL_PATH=/opt/conda

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH $INSTALL_PATH/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install --yes \
      wget \
      bzip2 \
      ca-certificates \
      libglib2.0-0 \
      libxext6 \
      libsm6 \
      libxrender1 \
      git \
      mercurial \
      subversion && \
    apt-get clean

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /root/anaconda.sh && \
    mkdir -p $(dirname $INSTALL_PATH) && \
    /bin/bash /root/anaconda.sh -b -p $INSTALL_PATH && \
    rm -f ~/anaconda.sh && \
    . $INSTALL_PATH/etc/profile.d/conda.sh && \
    conda update conda && \
    ln -s $INSTALL_PATH/etc/profile.d/conda.sh /etc/profile.d/conda.sh

COPY environment-lal-development.yml /tmp
RUN conda env create -f /tmp/environment-lal-development.yml
COPY activate.d/develop_lal.sh ${INSTALL_PATH}/envs/lal-development/etc/conda/activate.d/
RUN conda clean --all --yes

COPY entrypoint.sh /opt/entrypoint.sh
RUN chmod 0755 /opt/entrypoint.sh

ENTRYPOINT ["/opt/entrypoint.sh"]
