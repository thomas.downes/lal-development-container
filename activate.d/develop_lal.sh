#!/bin/sh

if [ $(uname -s) = "Darwin" ]; then
  DEFAULT_SDK_DIR=~/SDKs/MacOSX10.9.sdk
  if [ -d $DEFAULT_SDK_DIR ]; then
    export CONDA_BUILD_SYSROOT=$DEFAULT_SDK_DIR
  else
    echo "Download the MacOS 10.9 SDK to ${DEFAULT_SDK_DIR} from the tarball at"
    echo "https://github.com/phracker/MacOSX-SDKs/releases/tag/10.13"
  fi
fi

export LDFLAGS="$LDFLAGS,-rpath $CONDA_PREFIX/lib"
